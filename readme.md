# Setup
* Clone this repository `git clone https://gitlab.com/lukas_hellmann/mall-issue-tracker.git`
* Run `pip install -r requirements.txt`
* Make migrations `python manage.py migrate`
* Create superuser `python manage.py createsuperuser`
* Run server `python manage.py runserver`