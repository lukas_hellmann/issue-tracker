from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin, User, GroupAdmin, Group
from django.contrib.auth.admin import UserCreationForm

from .models import IssueUser, IssueGroup


class IssueUserCreationForm(UserCreationForm):

    class Meta:
        model = IssueUser
        fields = ('username', 'password')

    def save(self, commit=True):
        user = super().save(commit=False)
        user.is_staff = True
        if commit:
            user.save()
        return user


class IssueUserAdmin(UserAdmin):
    add_form = IssueUserCreationForm


admin.site.unregister(User)
admin.site.unregister(Group)
admin.site.register(IssueUser, IssueUserAdmin)
admin.site.register(IssueGroup, GroupAdmin)
