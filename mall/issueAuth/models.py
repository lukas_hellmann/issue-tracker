from django.contrib.auth.models import Group, User, UserManager
from django.utils.translation import ugettext_lazy as _


class IssueUserManager(UserManager):

    def create_user(self, username, email=None, password=None, **extra_fields):
        user = super(IssueUserManager, self).create_user(username, email, password, **extra_fields)
        user.is_staff = True
        user.save()
        return user


class IssueUser(User):
    objects = IssueUserManager()

    class Meta:
        proxy = True
        verbose_name_plural = _('Users')


class IssueGroup(Group):
    class Meta:
        proxy = True
        verbose_name_plural = _('Groups')
