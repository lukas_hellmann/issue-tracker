from django.test import TestCase
from django.db import IntegrityError

from .models import IssueUser


class IssueUserTestCase(TestCase):
    def setUp(self):
        IssueUser.objects.create_user(id=0, username='submitter1')

    def test_user_should_create_successfully(self):
        user = IssueUser.objects.get(id=0)
        self.assertEqual(user.username, 'submitter1')

    def test_user_should_be_staff_by_default(self):
        user = IssueUser.objects.get(id=0)
        self.assertEqual(user.is_staff, True)

    def test_user_creation_TypeError(self):
        with self.assertRaises(TypeError):
            IssueUser.objects.create_user()
