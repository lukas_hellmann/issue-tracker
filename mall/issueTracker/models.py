from django.db import models
from django.utils.translation import ugettext_lazy as _

from datetime import timedelta
from ..issueAuth.models import IssueUser


ISSUE_STATE = (
    (0, "Created"),
    (1, "Assigned"),
    (2, "Work in progress"),
    (3, "Solved"),
    (4, "Closed")
)


class CategoryManager(models.Manager):

    def create(self, name, **extra_fields):
        category = super().create(**{**extra_fields, 'name': name})
        category.save()
        return category


class Category(models.Model):
    name = models.CharField(max_length=30)
    objects = CategoryManager()

    class Meta:
        verbose_name_plural = _('Categories')

    def __str__(self):
        return self.name


class IssueManager(models.Manager):

    def create(self, title, description, **extra_fields):
        issue = super().create(**{**extra_fields, 'title': title, 'description': description})
        issue.save()
        return issue


class Issue(models.Model):
    submitter = models.ForeignKey(IssueUser, on_delete=models.CASCADE, related_name='issueTracker_submitter')
    assignee = models.ForeignKey(IssueUser, on_delete=models.CASCADE, related_name='issueTracker_assignee')
    title = models.CharField(max_length=200)
    description = models.TextField()
    state = models.IntegerField(choices=ISSUE_STATE, default=0)
    duration = models.DurationField(default=timedelta())
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='issueTracker_category')
    objects = IssueManager()

    def __str__(self):
        return self.title
