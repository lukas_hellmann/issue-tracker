from django.contrib import admin
from django.db.models import Max, Min, Avg
from .models import Issue, Category


class IssueAdmin(admin.ModelAdmin):
    list_display = ('title', 'submitter', 'duration', 'assignee', 'state', 'category')
    list_filter = ("category__name", "state", )
    search_fields = ['title', 'description', 'state', 'category__name']
    change_list_template = 'admin/issues/issues_change_list.html'

    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        issues = Issue.objects.all()
        extra_context['longest_solution'] = issues.aggregate(Max('duration'))['duration__max'] if issues else None
        extra_context['average_solution'] = issues.aggregate(Avg('duration'))['duration__avg'] if issues else None
        extra_context['shortest_solution'] = issues.aggregate(Min('duration'))['duration__min'] if issues else None
        return super(IssueAdmin, self).changelist_view(request, extra_context)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', )
    search_fields = ['name', ]


admin.site.register(Issue, IssueAdmin)
admin.site.register(Category, CategoryAdmin)
