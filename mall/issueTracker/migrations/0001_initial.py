# Generated by Django 2.2.4 on 2019-09-04 23:41

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('issueAuth', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30)),
            ],
            options={
                'verbose_name_plural': 'Categories',
            },
        ),
        migrations.CreateModel(
            name='Issue',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200)),
                ('description', models.TextField()),
                ('state', models.IntegerField(choices=[(0, 'Created'), (1, 'Assigned'), (2, 'Work in progress'), (3, 'Solved'), (4, 'Closed')], default=0)),
                ('duration', models.DurationField(default=datetime.timedelta(0))),
                ('assignee', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='issueTracker_assignee', to='issueAuth.IssueUser')),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='issueTracker_category', to='issueTracker.Category')),
                ('submitter', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='issueTracker_submitter', to='issueAuth.IssueUser')),
            ],
        ),
    ]
