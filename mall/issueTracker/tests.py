from django.test import TestCase
from django.db import IntegrityError

from .models import Category, Issue
from ..issueAuth.models import IssueUser


class CategoryCreationTestCase(TestCase):
    def setUp(self):
        Category.objects.create(id=0, name="Bug")

    def test_category_created_correctly(self):
        category = Category.objects.get(id=0)
        self.assertEqual(category.name, 'Bug')

    def test_category_creation_TypeError(self):
        with self.assertRaises(TypeError):
            Category.objects.create()

    def test_issue_duplication_IntegrityError(self):
        with self.assertRaises(IntegrityError):
            Category.objects.create(id=0, name="Bug")


class IssueTestCase(TestCase):
    def setUp(self):
        self.submitter = IssueUser.objects.create_user(id=0, username='submitter1')
        self.assignee = IssueUser.objects.create_user(id=1, username='assignee1')
        self.category = Category.objects.create(id=0, name="Bug")
        Issue.objects.create(id=0,
                             submitter=self.submitter,
                             assignee=self.assignee,
                             title="title",
                             description="desc",
                             category=self.category)

    def test_user_should_create_issue_successfully(self):
        issue = Issue.objects.get(id=0)
        self.assertEqual(issue.submitter.username, 'submitter1')
        self.assertEqual(issue.assignee.username, 'assignee1')
        self.assertEqual(issue.title, 'title')
        self.assertEqual(issue.description, 'desc')
        self.assertEqual(issue.category.name, 'Bug')

    def test_issue_creation_TypeError(self):
        with self.assertRaises(TypeError):
            Issue.objects.create()

    def test_issue_creation_IntegrityError(self):
        with self.assertRaises(IntegrityError):
            Issue.objects.create(title="title",
                                 description="desc")

    def test_issue_duplication_IntegrityError(self):
        with self.assertRaises(IntegrityError):
            Issue.objects.create(id=0,
                                 submitter=self.submitter,
                                 assignee=self.assignee,
                                 title="title",
                                 description="desc",
                                 category=self.category)
